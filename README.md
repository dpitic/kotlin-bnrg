# Introduction

Kotlin Programming: The Big Nerd Ranch Guide, 1st ed.

# Project structure

Each chapter in the book is placed in its own module named after the chapter in
the format `ch<nn>`, where `nn` is the chapter number.  Each module directory
has a source folder called `src` which contains the source code for that
module (and chapter).

## Packages

If required, packages can be created inside the `src` folder of a module 
(chapter), to further separate source code files.