// Compile-time constants; absolutely, positively immutable
const val MAX_EXPERIENCE: Int = 5000

// Program entry point
fun main(args: Array<String>) {
    // Kotlin supports type inference, so explicit type definitions are not
    // required.
    // Read-only variable (immutable)
    val playerName = "Estragon"
    // Declaring a variable
    var experiencePoints = 5
    println(experiencePoints)
    // A variable can be reassigned a new value
    experiencePoints += 5
    println(experiencePoints)
    println(playerName)
    println(playerName.reversed())
}